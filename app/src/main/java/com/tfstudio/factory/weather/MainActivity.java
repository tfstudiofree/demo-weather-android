package com.tfstudio.factory.weather;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    private TextView lblCityName;
    private TextView lblDescription;
    private TextView lblTemp;

    // temporary string to show the parsed response
    private String jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.lblCityName = (TextView) findViewById(R.id.lblCityName);
        this.lblDescription = (TextView) findViewById(R.id.lblDescription);
        this.lblTemp = (TextView) findViewById(R.id.lblTemp);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        determineMyCurrentLocation();
        //getWeatherFromApi("q=tucuman");
    }

    /**
     * Method to make json object request where json response
     * */
    private void getWeatherFromApi(String query) {

        String urlPath = String.format("http://api.openweathermap.org/data/2.5/weather?%s&lang=es&units=metric&APPID=18a3df6fc0a08acae2846519cfc6754f", query);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlPath, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray jsonArrayWeather = response.getJSONArray("weather");
                    for (int i = 0; i < jsonArrayWeather.length(); i++) {
                        JSONObject item = (JSONObject) jsonArrayWeather.get(i);
                        lblDescription.setText(item.getString("description"));
                    }

                    JSONObject objMain = response.getJSONObject("main");
                    lblTemp.setText("La temperatura es de: " + objMain.getString("temp") + "º");

                    lblCityName.setText(response.getString("name"));

                    jsonResponse = String.format("city: %s, description: %s, temp: %s", lblCityName.getText(), lblDescription.getText(), lblTemp.getText());
                    Log.d(TAG,jsonResponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void determineMyCurrentLocation() {

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                Log.d(TAG, String.format("latitude: %s, longitude: %s", location.getLatitude(), location.getLongitude()));
                //get weather from api using geolocalization
                getWeatherFromApi(String.format("lat=%s&lon=%s",location.getLatitude(),location.getLongitude()));

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},1);

            return;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }
}
